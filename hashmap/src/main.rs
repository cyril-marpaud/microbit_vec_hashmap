#![feature(restricted_std)]
#![no_main]

use std::{
	collections::{hash_map::DefaultHasher, HashMap},
	hash::BuildHasherDefault,
};

use cortex_m_rt::entry;
use embedded_alloc::Heap;
use microbit::{
	board::Board,
	hal::{prelude::*, timer::Timer},
};

#[global_allocator]
static HEAP: Heap = Heap::empty();

#[entry]
fn main() -> ! {
	{
		use core::mem::MaybeUninit;
		const HEAP_SIZE: usize = 8192; // 8KiB
		static mut HEAP_MEM: [MaybeUninit<u8>; HEAP_SIZE] = [MaybeUninit::uninit(); HEAP_SIZE];
		unsafe { HEAP.init(HEAP_MEM.as_ptr() as usize, HEAP_SIZE) }
	}

	let mut board = Board::take().expect("Failed to take board");
	let mut timer = Timer::new(board.TIMER0);
	let mut row = board.display_pins.row1;
	let delay = 150u16;

	board
		.display_pins
		.col1
		.set_low()
		.expect("Failed to set col1 low");

	let mut hm = HashMap::with_hasher(BuildHasherDefault::<DefaultHasher>::default());
	hm.insert(0, false);
	hm.insert(1, true);
	hm.insert(2, false);
	hm.insert(3, true);
	hm.insert(4, true);
	hm.insert(5, true);

	hm.values().cycle().for_each(|v| {
		match v {
			true => row.set_high().expect("Failed to set row high"),
			false => row.set_low().expect("Failed to set row low"),
		}
		timer.delay_ms(delay);
	});

	loop {}
}
