#![no_main]
#![no_std]

use cortex_m_rt::entry;
use microbit::{
	board::Board,
	hal::{prelude::*, timer::Timer},
};
use panic_halt as _;

#[entry]
fn main() -> ! {
	let mut board = Board::take().expect("Failed to take board");
	let mut timer = Timer::new(board.TIMER0);
	let mut row = board.display_pins.row1;
	let delay = 150u16;

	board
		.display_pins
		.col1
		.set_low()
		.expect("Failed to set col1 low");

	loop {
		row.set_high().expect("Failed to set row1 high");
		timer.delay_ms(delay);

		row.set_low().expect("Failed to set row1 low");
		timer.delay_ms(delay);
	}
}
