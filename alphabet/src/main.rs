#![feature(restricted_std)]
#![no_main]

mod alphabet;

use alphabet::Alphabet;
use cortex_m_rt::entry;
use embedded_alloc::Heap;
use microbit::{board::Board, display::blocking::Display, hal::timer::Timer};

#[global_allocator]
static HEAP: Heap = Heap::empty();

#[entry]
fn main() -> ! {
	{
		use core::mem::MaybeUninit;
		const HEAP_SIZE: usize = 8192; // 8KiB
		static mut HEAP_MEM: [MaybeUninit<u8>; HEAP_SIZE] = [MaybeUninit::uninit(); HEAP_SIZE];
		unsafe { HEAP.init(HEAP_MEM.as_ptr() as usize, HEAP_SIZE) }
	}

	let board = Board::take().expect("Failed to take board");
	let mut display = Display::new(board.display_pins);
	let mut timer = Timer::new(board.TIMER0);

	let alphabet = Alphabet::new();
	let delay = 150;

	loop {
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
			.chars()
			.for_each(|c| {
				display.show(&mut timer, *alphabet.get(c), delay);
			});
	}
}
